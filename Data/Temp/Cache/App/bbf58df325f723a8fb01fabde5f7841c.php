<?php if (!defined('THINK_PATH')) exit();?><footer  ><section id="footer-main"><div class="container"><div class="one-fourth column"><h4>关于我们</h4><hr class="smalline" /><p>绿荫工作室成立于2003年是由一群专业、独特的IT精英组成的软件工作室
				<br />
				绿荫的团队精神：不断完善自强、坚毅、稳健、自信.
				</p><div class="clear"></div><!-- <a style="padding-top: 15px; display: block;" href="about.html"><i
					class="icon-info-sign"></i> Read More </a> --></div><div class="one-fourth column"><h4>最新消息</h4><hr class="smalline" /><ul class="footer-recent-news"><?php if(is_array($newPosts)): $i = 0; $__LIST__ = $newPosts;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo (getsingleurlbyid($vo["post_id"])); ?>"
						title=" <?php echo ($vo["post_title"]); ?>"><i class="icon-circle-arrow-right"></i><?php echo (mb_substr($vo["post_title"],0,20,"UTF-8")); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?></ul></div><div class="one-fourth column"><h4>友情链接</h4><hr class="smalline" /><ul id="footer-recent-news"><?php if(is_array($friendurl)): $i = 0; $__LIST__ = $friendurl;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($vo["link_url"]); ?>" title=" <?php echo ($vo["link_name"]); ?>"><?php echo ($vo["link_name"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?></ul></div><div class="one-fourth column"><h4>联系方式</h4><hr class="smalline" /><ul class="getintouch"><li><i class="icon-map-marker"></i><strong>地址: </strong>信息学院学办620</li><li><i class="icon-phone"></i><strong>QQ群: </strong> 249301028
					</li><li><i class="icon-envelope"></i><strong>电子邮件: </strong>admin@njut.asia</li></ul></div></div></section><section class="footer-bar"><div class="container"><div class="nine  columns"><div class="footer-logo"><img src="__PUBLIC__/images/logo.png" alt="" title="" /><span>
						2003-<?php echo date('Y');?> © Copyright <a href="<?php echo C('site_url');?>"><?php echo (C("OUR_NAME")); ?></a>
						@ <a
						href="http://www.njut.edu.cn/">NJUT</a><br /><br />  &nbsp;&nbsp; &nbsp;   Powered By
						<a href="http://cms.njut.asia/">GreenCMS V1</a>
						&nbsp;&nbsp;&nbsp;<a href="<?php echo U('Admin/Login/index');?>">管理入口</a></span></div></div><div class="seven columns"><ul class="footer-links"><li><a href="http://page.renren.com/601038207"><i class="icon-twitter"></i></a></li><li><a
						href="https://www.facebook.com/pages/Green-Studio/504741962894684"><i
							class="icon-facebook"></i></a></li><li><a href="#"><i class="icon-google-plus"></i></a></li></ul></div></div></section></footer><!-- ==================================================
Footer / End -->