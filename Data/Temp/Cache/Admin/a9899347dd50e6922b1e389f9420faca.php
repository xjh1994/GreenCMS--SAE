<?php if (!defined('THINK_PATH')) exit(); echo ($head_meta); ?><title><?php echo ($action); ?> | <?php echo (C("title")); ?></title><script type="text/javascript">	window.UEDITOR_HOME_URL = "__PUBLIC__/Ueditor/";

	var URL_upload="<?php echo U('Admin/File/imageUp');?>";
	var URL_fileUp="<?php echo U('Admin/File/fileUp');?>";
	var URL_scrawlUp="<?php echo U('Admin/File/scrawlUp');?>";
	var URL_getRemoteImage="<?php echo U('Admin/File/getRemoteImage');?>";
	var URL_imageManager="<?php echo U('Admin/File/imageManager');?>";
	
	var URL_imageUp="<?php echo U('Admin/File/imageUp');?>";

	var URL_getMovie="<?php echo U('Admin/File/getMovie');?>";

	
	
	
	var URL_home="";
	
</script><script type="text/javascript" src="__PUBLIC__/Ueditor/ueditor.config.js"></script><script type="text/javascript" src="__PUBLIC__/Ueditor/ueditor.all.min.js"></script></head><!-- END HEAD --><!-- BEGIN BODY --><body><!-- BEGIN HEADER --><div class="header navbar navbar-inverse navbar-static-top"><!-- BEGIN TOP NAVIGATION BAR --><div class="navbar-inner"><div class="container-fluid"><!-- BEGIN LOGO --><a class="brand" href="<?php echo U('Admin/Index/index');?>"><img
					src="__PUBLIC__/admin/assets/img/logo.png" alt="logo" /></a><!-- END LOGO --><!-- BEGIN RESPONSIVE MENU TOGGLER --><a href="javascript:;" class="btn-navbar collapsed"
					data-toggle="collapse" data-target=".nav-collapse"><img
					src="__PUBLIC__/admin/assets/img/menu-toggler.png" alt="" /></a><!-- END RESPONSIVE MENU TOGGLER --><!-- BEGIN TOP NAVIGATION MENU --><ul class="nav pull-right"><!-- BEGIN USER LOGIN DROPDOWN --><li class="dropdown user"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"><img alt=""
							src="__PUBLIC__/admin/assets/img/avatar1_small.jpg" /><span
							class="username"><?php echo ($username); ?></span><i class="icon-angle-down"></i></a><ul class="dropdown-menu"><li><a href="<?php echo U('Admin/Index/index');?>"><i
									class="icon-user"></i>个人信息</a></li><li class="divider"></li><li><a href="<?php echo U('Admin/Login/logout');?>"><i
									class="icon-key"></i>退出</a></li></ul></li><!-- END USER LOGIN DROPDOWN --></ul><!-- END TOP NAVIGATION MENU --></div></div><!-- END TOP NAVIGATION BAR --></div><!-- END HEADER --><!-- BEGIN CONTAINER --><div class="page-container row-fluid"><!-- BEGIN SIDEBAR --><div class="page-sidebar nav-collapse collapse"><!-- BEGIN SIDEBAR MENU --><ul class="page-sidebar-menu"><li><!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler hidden-phone"></div><!-- BEGIN SIDEBAR TOGGLER BUTTON --></li><?php echo ($menu); ?></ul><!-- END SIDEBAR MENU --></div><!-- END SIDEBAR --><!-- BEGIN PAGE --><div class="page-content"><!-- BEGIN PAGE CONTAINER--><div class="container-fluid"><!-- BEGIN PAGE HEADER--><div class="row-fluid"><div class="span12"><!-- BEGIN PAGE TITLE & BREADCRUMB--><h3 class="page-title"><?php echo ($action); ?><small>&nbsp;&nbsp;<?php echo (C("title")); ?></small></h3><ul class="breadcrumb"><li><a href="<?php echo ($module_url); ?>"><?php echo ($module); ?></a><i
								class="icon-angle-right"></i></li><li><a href="<?php echo ($action_url); ?>"><?php echo ($action); ?></a></li></ul><!-- END PAGE TITLE & BREADCRUMB--></div></div><!-- END PAGE HEADER--><!-- BEGIN PAGE CONTENT--><div class="row-fluid"><div class="span12"><!-- BEGIN EXAMPLE TABLE PORTLET--><form action="<?php echo U('Posts/addHandle');?>" id="add_post"
							name="add_post" class="form-horizontal"><div class="control-group"><label class="control-label">标题</label><div class="controls"><input type="text" id="post_title" placeholder=""
										class="m-wrap medium" name="post_title"
										value="<?php echo ($info["post_title"]); ?>" /><span class="help-inline"></span></div></div><div class="control-group"><label class="control-label">内容</label><textarea class="span12 ckeditor m-wrap" id="post_content"
									name="post_content" style="width: 60%; margin-left: 20px;"><?php echo ($info["post_content"]); ?></textarea></div><div class="control-group"><label class="control-label">分类</label><div class="controls"><?php if(is_array($cats)): foreach($cats as $key=>$vCats): ?><input
										type="checkbox" value="<?php echo ($vCats["cat_id"]); ?>" name="cats[]"
									<?php if(is_array($info['pcs'])): foreach($info['pcs'] as $key=>$pcs): if($vCats['cat_id'] == $pcs['cat_id']): ?>checked="checked" <?php else: endif; endforeach; endif; ?> /><?php echo ($vCats["cat_name"]); ?>&nbsp;&nbsp;<?php endforeach; endif; ?></div></div><div class="control-group"><label class="control-label">标签</label><div class="controls"><?php if(is_array($tags)): foreach($tags as $key=>$vTags): ?><input
										type="checkbox" value="<?php echo ($vTags["tag_id"]); ?>" name="tags[]"
									<?php if(is_array($info['pts'])): foreach($info['pts'] as $key=>$pts): if($vTags['tag_id'] == $pts['tag_id']): ?>checked="checked" <?php else: endif; endforeach; endif; ?> /><?php echo ($vTags["tag_name"]); ?>&nbsp;&nbsp;<?php endforeach; endif; ?></div></div><div class="control-group"><label class="control-label">分类</label><div class="controls"><label class="radio"><div class="radio"><?php if($info["post_type"] == single ): ?><span class="checked"><input type="radio"
												name="post_type" value="single" checked=""></span><?php elseif($info["post_type"] == page ): ?><span class=""><input type="radio"
												name="post_type" value="single" ></span><?php else: ?><span class="checked"><input type="radio"
												name="post_type" value="single" checked=""></span><?php endif; ?></div> 文章
									</label><label class="radio"><div class="radio"><?php if($info["post_type"] == page ): ?><span class="checked"><input type="radio"
												name="post_type" value="page" checked="" ></span><?php else: ?><span class=""><input type="radio"
												name="post_type" value="page" ></span><?php endif; ?></div> 页面
									</label></div></div><div class="control-group"><label class="control-label">置顶</label><div class="controls"><label class="radio"><div class="radio"><?php if($info["post_top"] == 1 ): ?><span class="checked"><input type="radio"
												name="post_top" value="1" checked=""></span><?php else: ?><span class=""><input type="radio"
												name="post_top" value="1" ></span><?php endif; ?></div> 是
									</label><label class="radio"><div class="radio"><?php if($info["post_top"] == 0 ): ?><span class="checked"><input type="radio"
												name="post_top" value="0" checked="" ></span><?php else: ?><span class=""><input type="radio"
												name="post_top" value="0" ></span><?php endif; ?></div> 否
									</label></div></div><input type="hidden" name="post_id" value="<?php echo ($post_id); ?>" /><div class="form-actions"><span style="font-size: 18px; color: red; font-weight: bold;"
									id="success"></span><button type="button" class="btn blue submit"><i class="icon-ok"></i> 发布
								</button></div><?php switch($action_name): case "posts": ?><span id="action" style="display: none;">posts</span><?php break; case "add": ?><span id="action" style="display: none;">addHandle</span><?php break; default: ?><span id="action" style="display: none;">addHandle</span><?php endswitch;?></form><!-- END EXAMPLE TABLE PORTLET--></div></div><!-- END PAGE CONTENT--></div><!-- END PAGE CONTAINER--></div><!-- END PAGE --></div><!-- END CONTAINER --><?php echo ($footer); echo ($foot_js); ?><script>		jQuery(document).ready(function() {
			App.init();
			TableManaged.init();
		});
	</script><script type="text/javascript">		$(function() {

			var editor;
			//具体参数配置在  editor_config.js  中
			var options = {
				initialFrameWidth : 800, //初化宽度
				initialFrameHeight : 400, //初化高度
				focus : false, //初始化时，是否让编辑器获得焦点true或false
				maximumWords : 99999, //允许的最大字符数 'fullscreen',
				toolbars : [ [ 'fullscreen', 'source', '|', 'undo', 'redo',
						'|', 'bold', 'italic', 'underline', 'fontborder',
						'strikethrough', 'superscript', 'subscript',
						'removeformat', 'formatmatch', 'autotypeset',
						'blockquote', 'pasteplain', '|', 'forecolor',
						'backcolor', 'insertorderedlist',
						'insertunorderedlist', 'selectall', 'cleardoc', '|',
						'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
						'customstyle', 'paragraph', 'fontfamily', 'fontsize',
						'|', 'directionalityltr', 'directionalityrtl',
						'indent', '|', 'justifyleft', 'justifycenter',
						'justifyright', 'justifyjustify', '|', 'touppercase',
						'tolowercase', '|', 'link', 'unlink', 'anchor', '|',
						'imagenone', 'imageleft', 'imageright', 'imagecenter',
						'|', 'insertimage', 'emotion', 'scrawl', 'insertvideo',
						'music', 'attachment', 'map', 'gmap', 'insertframe',
						'insertcode', 'webapp', 'pagebreak', 'template',
						'background', '|', 'horizontal', 'date', 'time',
						'spechars',  'wordimage', '|',
						'inserttable', 'deletetable',
						'insertparagraphbeforetable', 'insertrow', 'deleterow',
						'insertcol', 'deletecol', 'mergecells', 'mergeright',
						'mergedown', 'splittocells', 'splittorows',
						'splittocols', '|', 'print', 'preview', 'searchreplace' ] ],

			};
			editor = new UE.ui.Editor(options);
			editor.render("post_content");
			/*editor.ready(function(){
			    editor.setContent('<?php echo ($v["content"]); ?>');
			});*/

			$(".submit").click(
					function() {

						var div = $("#success");

						if ($("post_title").val() == ''
								|| $("#post_content").val() == '') {
							div.fadeIn().html("标题或内容不能为空");
							div.fadeOut(3000);
							return false;
						}
						var action = $("#action").html();
						var url = '__URL__/' + action;
						var formObj = $("#add_post");
						formObj.ajaxSubmit({
							url : url,
							type : "POST",
							dataType : "json",
							success : function(data) {
								if (data.status == 1) {
									$(".submit").hide();
									div.fadeIn().html(data.info);
									div.fadeOut(3000);

								} else {
									div.fadeIn().html(data.info);
									div.fadeOut(5000);
								}
								if (data.url && data.url != '') {
									setTimeout(function() {
										top.window.location.href = data.url;
									}, 1000);
								}
								if (data.url == '') {
									setTimeout(function() {
										top.window.location.reload();
									}, 1000);
								}
							}
						});
					});
		});
	</script></body><!-- END BODY --></html>