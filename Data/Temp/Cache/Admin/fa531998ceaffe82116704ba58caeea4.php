<?php if (!defined('THINK_PATH')) exit(); echo ($head_meta); ?><title><?php echo ($action); ?> | <?php echo (C("title")); ?></title></head><!-- END HEAD --><!-- BEGIN BODY --><body class="page-header-fixed"><!-- BEGIN HEADER --><div class="header navbar navbar-inverse navbar-fixed-top"><!-- BEGIN TOP NAVIGATION BAR --><div class="navbar-inner"><div class="container-fluid"><!-- BEGIN LOGO --><a class="brand" href="<?php echo U('Admin/Index/index');?>"><img src="__PUBLIC__/admin/assets/img/logo.png" alt="logo" /></a><!-- END LOGO --><!-- BEGIN RESPONSIVE MENU TOGGLER --><a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse"><img src="__PUBLIC__/admin/assets/img/menu-toggler.png" alt="" /></a><!-- END RESPONSIVE MENU TOGGLER --><!-- BEGIN TOP NAVIGATION MENU --><ul class="nav pull-right"><!-- BEGIN USER LOGIN DROPDOWN --><li class="dropdown user"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="" src="__PUBLIC__/admin/assets/img/avatar1_small.jpg" /><span class="username"><?php echo ($username); ?></span><i class="icon-angle-down"></i></a><ul class="dropdown-menu"><li><a href="<?php echo U('Admin/Index/index');?>"><i class="icon-user"></i>个人信息</a></li><li class="divider"></li><li><a href="<?php echo U('Admin/Login/logout');?>"><i class="icon-key"></i>退出</a></li></ul></li><!-- END USER LOGIN DROPDOWN --></ul><!-- END TOP NAVIGATION MENU --></div></div><!-- END TOP NAVIGATION BAR --></div><!-- END HEADER --><!-- BEGIN CONTAINER --><div class="page-container row-fluid"><!-- BEGIN SIDEBAR --><div class="page-sidebar nav-collapse collapse"><!-- BEGIN SIDEBAR MENU --><ul class="page-sidebar-menu"><li><!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler hidden-phone"></div><!-- BEGIN SIDEBAR TOGGLER BUTTON --></li><?php echo ($menu); ?></ul><!-- END SIDEBAR MENU --></div><!-- END SIDEBAR --><!-- BEGIN PAGE --><div class="page-content"><!-- BEGIN PAGE CONTAINER--><div class="container-fluid"><!-- BEGIN PAGE HEADER--><div class="row-fluid"><div class="span12"><!-- BEGIN PAGE TITLE & BREADCRUMB--><h3 class="page-title"><?php echo ($action); ?><small>&nbsp;&nbsp;<?php echo (C("title")); ?></small></h3><ul class="breadcrumb"><li><a href="<?php echo ($module_url); ?>"><?php echo ($module); ?></a><i class="icon-angle-right"></i></li><li><a href="<?php echo ($action_url); ?>"><?php echo ($action); ?></a></li></ul><!-- END PAGE TITLE & BREADCRUMB--></div></div><!-- END PAGE HEADER--><!-- BEGIN PAGE CONTENT--><div class="row-fluid"><div class="span12"><h4>共有<?php echo ($files); ?>个压缩包文件，共计<?php echo ($total); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red;font-weight:bold;" id="success"></span></h4><!-- BEGIN EXAMPLE TABLE PORTLET--><form method="post"><table
                                class="table table-striped table-bordered table-hover"><thead><tr><th style="width: 8px;">选择</th><th >压缩包名称</th><th class="hidden-240">打包时间</th><th class="hidden-480">文件大小</th><th class="">解压</th></tr></thead><tbody id="cache_table"><?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$zip): $mod = ($i % 2 );++$i;?><tr id="<?php echo (del_zip($zip["file"])); ?>" align="center"><td><input type="checkbox" name="zipFiles" value="<?php echo ($zip["file"]); ?>"/></td><td align="left"><a href="<?php echo U('Sysdata/downFile',array('file'=>$zip['file'],'type'=>'zip'));?>" target="_blank"><?php echo ($zip["file"]); ?></a></td><td><?php echo ($zip["time"]); ?></td><td><?php echo ($zip["size"]); ?></td><td><button class="btn unzip" file="<?php echo ($zip["file"]); ?>">解压</button></td></tr><?php endforeach; endif; else: echo "" ;endif; ?></tbody><tfoot><tr><th></th><th></th><th></th><th></th><th></th></tr></tfoot></table><div class="form-actions" style="text-align: center"><div class="span2"><button type="button" class="btn green delZipFiles"><i class="m-icon-swapright m-icon-white"></i>删除所选
                                    </button></div><div class="span2"><button type="button" class="btn green unzipSelect"><i class="m-icon-swapright m-icon-white"></i>解压缩所选
                                    </button></div><div class="span2"><button type="button" class="btn" id="checkall">全选</button></div><div class="span2"><button type="button" class="btn red" id="check_cancel">取消</button></div><div class="span2"><button type="button" class="btn blue" id="check_reverse">反选</button></div></div></form><!-- END EXAMPLE TABLE PORTLET--></div></div><!-- END PAGE CONTENT--></div><!-- END PAGE CONTAINER--></div><!-- END PAGE --></div><!-- END CONTAINER --><?php echo ($footer); echo ($foot_js); ?><script>
        jQuery(document).ready(function() {      
            $("#cache_table :checkbox").attr("checked", true);
             
            App.init();
            TableManaged.init();

            $("#checkall").click(function() { //":checked"匹配所有的复选框
                $("#cache_table :checkbox").attr("checked", true); //"#div1 :checked"之间必须有空格checked是设置选中状态。如果为true则是选中fo否则false为不选中
                $("span").addClass("checked"); 
            });
            $("#check_cancel").click(function() {
                $("#cache_table :checkbox").attr("checked", false);
                $("span").removeClass("checked"); 
            });
            //理解用迭代原理each（function(){}）
            $("#check_reverse").click(function() {
                $("#cache_table :checkbox").each(function() {
                
                    $(this).attr("checked", !$(this).attr("checked")); 
                    $(this).parentsUntil('div').toggleClass("checked");
                    
                    
                    //!$(this).attr("checked")判断复选框的状态：如果选中则取反
                });
            });

            //delZipFiles
            $(".delZipFiles").click(function(){
                var obj=document.getElementsByName('zipFiles');
                // var file = [];
                var zipFiles = '';
                for(var i = 0; i < obj.length; i++) {
                    if(obj[i].checked) {
                       // file.push({zipFiles:obj[i].value}); 
                       if(i != obj.length - 1) {
                            zipFiles = zipFiles + (obj[i].value) + ',';
                       }
                       else {
                        zipFiles += obj[i].value;
                       }
                   }
                }
                var url = "<?php echo U('Admin/Sysdata/delzipFiles');?>";
                $.post(url, {zipFiles:zipFiles}, function(data) {
                    
                    //删除成功
                    if(data.status == 1) {
                        $("#success").fadeIn().html(data.info);
                        $("#success").fadeOut(3000);

                        var to_deleted_obj=document.getElementsByName('zipFiles');
                        var to_deleted_zipFiles = '';
                        for(var i = 0; i < to_deleted_obj.length; i++) {
                            if(to_deleted_obj[i].checked) {
                               // file.push({zipFiles:obj[i].value}); 
                               if(i != to_deleted_obj.length - 1) {
                                    to_deleted_zipFiles = to_deleted_zipFiles + (to_deleted_obj[i].value) + ',';
                               }
                               else {
                                    to_deleted_zipFiles += to_deleted_obj[i].value;
                               }
                           }
                        }
                        var deleted = to_deleted_zipFiles.split(',');

                        $.each(deleted, function(n, value) {
                            var name = value.replace(".zip", "");
                            var to_deleted = $('#' + name);
                            to_deleted.fadeOut();
                        });
                    }
                    else {
                        $("#success").fadeIn().html(data.info);
                        $("#success").fadeOut(3000);
                    }
                }, "json");
                
            });
            
            $(".unzip").click(function(){
            	var url_unzip = "<?php echo U('Admin/Sysdata/unzipSqlfile');?>";
                $.post(url_unzip,{'zipFiles':$(this).attr("file")},function(json){
                    var json = eval("(" + json + ")"); //真拙计作者居然注释掉了这个。。。。

					//alert(json.info);
                  
                    $(".btn").removeAttr("disabledSubmit");
                    if(json.status == 1) {
                       	$("#success").fadeIn().html(json.info);
                        $("#success").fadeOut(3000);
                        location.href = json.url;
                    }
                    else {
                        $("#success").fadeIn().html(json.info);
                        $("#success").fadeOut(3000);
                    }
                });
                return false;
            });
            
          //unzipSelect
            $(".unzipSelect").click(function(){
            	if($("tbody input[type='checkbox']:checked").size()==0){
            		$("#success").fadeIn().html("请选择你要解压的数据库");
            		$("#success").fadeOut(2000);
                    return false;
                }
            	
                var obj2=document.getElementsByName('zipFiles');
                // var file = [];
                var zipFiles2 = '';
                for(var i = 0; i < obj2.length; i++) {
                    if(obj2[i].checked) {
                       // file.push({sqlFiles:obj[i].value}); 
                       if(i != obj2.length - 1) {
                    	   zipFiles2 = zipFiles2 + (obj2[i].value) + ',';
                       }
                       else {
                        zipFiles2 += obj2[i].value;
                       }
                   }
                }
                var url2 = "<?php echo U('Admin/Sysdata/unzipSqlfile');?>";
                $.post(url2, {zipFiles:zipFiles2}, function(data) {
                    
                    //解压成功
                    if(data.status == 1) {
                       	$("#success").fadeIn().html(data.info);
                        $("#success").fadeOut(3000);
                        location.href = data.url;
                    }
                    else {
                        $("#success").fadeIn().html(data.info);
                        $("#success").fadeOut(3000);
                    }
                }, "json");
            });
        });
    </script></body><!-- END BODY --></html>