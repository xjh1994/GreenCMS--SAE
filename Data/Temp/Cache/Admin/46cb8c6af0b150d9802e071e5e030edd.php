<?php if (!defined('THINK_PATH')) exit(); echo ($head_meta); ?><title><?php echo ($action); ?> | <?php echo (C("title")); ?></title></head><!-- END HEAD --><!-- BEGIN BODY --><body class="page-header-fixed"><!-- BEGIN HEADER --><div class="header navbar navbar-inverse navbar-fixed-top"><!-- BEGIN TOP NAVIGATION BAR --><div class="navbar-inner"><div class="container-fluid"><!-- BEGIN LOGO --><a class="brand" href="<?php echo U('Admin/Index/index');?>"><img
					src="__PUBLIC__/admin/assets/img/logo.png" alt="logo" /></a><!-- END LOGO --><!-- BEGIN RESPONSIVE MENU TOGGLER --><a href="javascript:;" class="btn-navbar collapsed"
					data-toggle="collapse" data-target=".nav-collapse"><img
					src="__PUBLIC__/admin/assets/img/menu-toggler.png" alt="" /></a><!-- END RESPONSIVE MENU TOGGLER --><!-- BEGIN TOP NAVIGATION MENU --><ul class="nav pull-right"><!-- BEGIN USER LOGIN DROPDOWN --><li class="dropdown user"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"><img alt=""
							src="__PUBLIC__/admin/assets/img/avatar1_small.jpg" /><span
							class="username"><?php echo ($username); ?></span><i class="icon-angle-down"></i></a><ul class="dropdown-menu"><li><a href="<?php echo U('Admin/Index/index');?>"><i
									class="icon-user"></i>个人信息</a></li><li class="divider"></li><li><a href="<?php echo U('Admin/Login/logout');?>"><i
									class="icon-key"></i>退出</a></li></ul></li><!-- END USER LOGIN DROPDOWN --></ul><!-- END TOP NAVIGATION MENU --></div></div><!-- END TOP NAVIGATION BAR --></div><!-- END HEADER --><!-- BEGIN CONTAINER --><div class="page-container row-fluid"><!-- BEGIN SIDEBAR --><div class="page-sidebar nav-collapse collapse"><!-- BEGIN SIDEBAR MENU --><ul class="page-sidebar-menu"><li><!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler hidden-phone"></div><!-- BEGIN SIDEBAR TOGGLER BUTTON --></li><?php echo ($menu); ?></ul><!-- END SIDEBAR MENU --></div><!-- END SIDEBAR --><!-- BEGIN PAGE --><div class="page-content"><!-- BEGIN PAGE CONTAINER--><div class="container-fluid"><!-- BEGIN PAGE HEADER--><div class="row-fluid"><div class="span12"><!-- BEGIN PAGE TITLE & BREADCRUMB--><h3 class="page-title"><?php echo ($action); ?><small>&nbsp;&nbsp;<?php echo (C("title")); ?></small></h3><ul class="breadcrumb"><li><a href="<?php echo ($module_url); ?>"><?php echo ($module); ?></a><i
								class="icon-angle-right"></i></li><li><a href="<?php echo ($action_url); ?>"><?php echo ($action); ?></a></li></ul><!-- END PAGE TITLE & BREADCRUMB--></div></div><!-- END PAGE HEADER--><!-- BEGIN PAGE CONTENT--><div class="row-fluid"><div class="span12"><!-- BEGIN EXAMPLE TABLE PORTLET--><?php switch($action_name): case "editUser": ?><form action="<?php echo U('Admin/Access/editUser');?>"
							class="form-horizontal" id="newuser" method="post"><?php break; default: ?><form action="<?php echo U('Admin/Access/addUserHandle');?>"
								class="form-horizontal" id="newuser" method="post"><?php endswitch;?><input type="text" style="display:none" name="user_id0" value="<?php echo ($info["user_id"]); ?>"/><div class="control-group"><label class="control-label">登录名</label><div class="controls"><input type="text" placeholder="登录名" class="m-wrap medium"
										name="user_login" value="<?php echo ($info["user_login"]); ?>"/></div></div><div class="control-group"><label class="control-label">密码</label><div class="controls"><?php switch($action_name): case "editUser": ?><input type="password" placeholder="******"
										class="m-wrap medium" name="password"  /><?php break; default: ?><input type="password" placeholder="******"
										class="m-wrap medium" name="password" id="password" /><?php endswitch;?><span
										class="help-inline" ></span></div></div><?php switch($action_name): case "addUser": ?><div class="control-group"><label class="control-label">确认密码</label><div class="controls"><input type="password" placeholder="******"
										class="m-wrap medium" name="rpassword" id="rpassword" /><span
										class="help-inline"></span></div></div><?php break; default: endswitch;?><div class="control-group"><label class="control-label">昵称</label><div class="controls"><input type="text" placeholder="昵称" class="m-wrap medium"
										name="user_nicename" value="<?php echo ($info["user_nicename"]); ?>" /></div></div><div class="control-group"><label class="control-label">显示名称</label><div class="controls"><input type="text" placeholder="显示名称" class="m-wrap medium"
										name="display_name" value="<?php echo ($info["user_nicename"]); ?>"/></div></div><div class="control-group"><label class="control-label">邮箱</label><div class="controls"><input type="text" placeholder="邮箱" class="m-wrap medium"
										name="user_email" value="<?php echo ($info["user_email"]); ?>"/><!--<span class="help-inline">“别名”是在URL中使用的别称，它可以令URL更美观。通常使用小写，只能包含字母，数字和连字符（-）。</span> --></div></div><div class="control-group"><label class="control-label">主页</label><div class="controls"><input type="text" placeholder="主页" class="m-wrap medium"
										name="user_url"  value="<?php echo ($info["user_url"]); ?>"/></div></div><div class="control-group"><label class="control-label">简介</label><div class="controls"><input type="text" placeholder="简介" class="m-wrap medium"
										name="user_intro"  value="<?php echo ($info["user_intro"]); ?>"/></div></div><div class="control-group"><label class="control-label">状态：</label><div class="controls"><select name="user_status" class="medium m-wrap" tabindex="1"><?php if($info["user_status"] == 1): ?><option value="1" selected>启用</option><option value="0">禁用</option><?php else: ?><option value="1">启用</option><option value="0" selected>禁用</option><?php endif; ?></select></div></div><div class="control-group"><label class="control-label">所属用户组</label><div class="controls"><select name="role_id" class="medium m-wrap" tabindex="1"><?php echo ($info["roleOption"]); ?></select></div></div><div class="form-actions"><a href="javascript:;" class="btn blue" id="button"><i
									class="icon-ok"></i><?php switch($action_name): case "editUser": ?>修改<?php break; case "addUser": ?>添加<?php break; default: ?>添加<?php endswitch;?></a></div></form><!-- END EXAMPLE TABLE PORTLET--></div></div><!-- END PAGE CONTENT--></div><!-- END PAGE CONTAINER--></div><!-- END PAGE --></div><!-- END CONTAINER --><?php echo ($footer); echo ($foot_js); ?><script>
		jQuery(document).ready(function() {
			App.init();
			TableManaged.init();
		});

		$("#button").click(
				function() {
					if ($("#password").val() == ''
							|| $("input[name='user_login']").val() == ''
							|| $("input[name='user_email']").val() == ''
							|| $("select[name='role_id']").val() == '') {
						alert("信息不完整！");
					} else if ($("#password").val() != $("#rpassword").val()) {
						alert("两次密码输入不一致！");
					} else {
						$("#newuser").submit();
					}
				});
	</script></body><!-- END BODY --></html>