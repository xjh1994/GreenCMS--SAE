<?php if (!defined('THINK_PATH')) exit(); echo ($head_meta); ?><title><?php echo ($action); ?> | <?php echo (C("title")); ?></title></head><!-- END HEAD --><!-- BEGIN BODY --><body class="page-header-fixed"><!-- BEGIN HEADER --><div class="header navbar navbar-inverse navbar-fixed-top"><!-- BEGIN TOP NAVIGATION BAR --><div class="navbar-inner"><div class="container-fluid"><!-- BEGIN LOGO --><a class="brand" href="<?php echo U('Admin/Index/index');?>"><img src="__PUBLIC__/admin/assets/img/logo.png" alt="logo" /></a><!-- END LOGO --><!-- BEGIN RESPONSIVE MENU TOGGLER --><a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse"><img src="__PUBLIC__/admin/assets/img/menu-toggler.png" alt="" /></a><!-- END RESPONSIVE MENU TOGGLER --><!-- BEGIN TOP NAVIGATION MENU --><ul class="nav pull-right"><!-- BEGIN USER LOGIN DROPDOWN --><li class="dropdown user"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="" src="__PUBLIC__/admin/assets/img/avatar1_small.jpg" /><span class="username"><?php echo ($username); ?></span><i class="icon-angle-down"></i></a><ul class="dropdown-menu"><li><a href="<?php echo U('Admin/Index/index');?>"><i class="icon-user"></i>个人信息</a></li><li class="divider"></li><li><a href="<?php echo U('Admin/Login/logout');?>"><i class="icon-key"></i>退出</a></li></ul></li><!-- END USER LOGIN DROPDOWN --></ul><!-- END TOP NAVIGATION MENU --></div></div><!-- END TOP NAVIGATION BAR --></div><!-- END HEADER --><!-- BEGIN CONTAINER --><div class="page-container row-fluid"><!-- BEGIN SIDEBAR --><div class="page-sidebar nav-collapse collapse"><!-- BEGIN SIDEBAR MENU --><ul class="page-sidebar-menu"><li><!-- BEGIN SIDEBAR TOGGLER BUTTON --><div class="sidebar-toggler hidden-phone"></div><!-- BEGIN SIDEBAR TOGGLER BUTTON --></li><?php echo ($menu); ?></ul><!-- END SIDEBAR MENU --></div><!-- END SIDEBAR --><!-- BEGIN PAGE --><div class="page-content"><!-- BEGIN PAGE CONTAINER--><div class="container-fluid"><!-- BEGIN PAGE HEADER--><div class="row-fluid"><div class="span12"><!-- BEGIN PAGE TITLE & BREADCRUMB--><h3 class="page-title"><?php echo ($action); ?><small>&nbsp;&nbsp;<?php echo (C("title")); ?></small></h3><ul class="breadcrumb"><li><a href="<?php echo ($module_url); ?>"><?php echo ($module); ?></a><i class="icon-angle-right"></i></li><li><a href="<?php echo ($action_url); ?>"><?php echo ($action); ?></a></li></ul><!-- END PAGE TITLE & BREADCRUMB--></div></div><!-- END PAGE HEADER--><!-- BEGIN PAGE CONTENT--><div class="row-fluid"><div class="span12"><h4>数据库中共有<?php echo ($tables); ?>张表，共计<?php echo ($total); ?></h4><!-- BEGIN EXAMPLE TABLE PORTLET--><form method="post" action="<?php echo U('Admin/Sysdata/backup');?>"><table
                                class="table table-striped table-bordered table-hover"><thead><tr><th style="width: 8px;">选择</th><th >表名</th><th class="hidden-240">表用途</th><th class="hidden-480">记录行数</th><th class="">引擎类型</th><th class="">字符集</th><th class="">表大小</th></tr></thead><tbody id="cache_table"><?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tab): $mod = ($i % 2 );++$i;?><tr align="center"><td><input type="checkbox" class='groupclass' name="table[]" value="<?php echo ($tab["Name"]); ?>"/></td><td align="left"><?php echo ($tab["Name"]); ?></td><td><?php echo ($tab["Comment"]); ?></td><td><?php echo ($tab["Rows"]); ?></td><td><?php echo ($tab["Engine"]); ?></td><td><?php echo ($tab["Collation"]); ?></td><td><?php echo ($tab["size"]); ?></td></tr><?php endforeach; endif; else: echo "" ;endif; ?></tbody><tfoot><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr></tfoot></table><div class="form-actions" style="text-align: center"><div class="span4"><button type="submit" class="btn green"><i class="m-icon-swapright m-icon-white"></i>开始备份
                                    </button></div><div class="span2"><button type="button" class="btn" id="checkall">全选</button></div><div class="span2"><button type="button" class="btn red" id="check_cancel">取消</button></div><div class="span2"><button type="button" class="btn blue" id="check_reverse">反选</button></div></div></form><!-- END EXAMPLE TABLE PORTLET--></div></div><!-- END PAGE CONTENT--></div><!-- END PAGE CONTAINER--></div><!-- END PAGE --></div><!-- END CONTAINER --><?php echo ($footer); echo ($foot_js); ?><script>
        jQuery(document).ready(function() {      
            $("#cache_table :checkbox").attr("checked", true);
             
            App.init();
            TableManaged.init();

            $("#checkall").click(function() { //":checked"匹配所有的复选框
                $("#cache_table :checkbox").attr("checked", true); //"#div1 :checked"之间必须有空格checked是设置选中状态。如果为true则是选中fo否则false为不选中
                $("span").addClass("checked"); 
            });
            $("#check_cancel").click(function() {
                $("#cache_table :checkbox").attr("checked", false);
                $("span").removeClass("checked"); 
            });
            //理解用迭代原理each（function(){}）
            $("#check_reverse").click(function() {
                $("#cache_table :checkbox").each(function() {
                
                    $(this).attr("checked", !$(this).attr("checked")); 
                    $(this).parentsUntil('div').toggleClass("checked");
                    
                    
                    //!$(this).attr("checked")判断复选框的状态：如果选中则取反
                });
            });
        });
    </script></body><!-- END BODY --></html>