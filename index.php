<?php

/**
 * @author ZTS
 * @version GreenCMS v1.13.1016
 *  
 */

//error_reporting(0);
@set_time_limit(240);
//@ini_set("memory_limit",'-1');
define('ENGINE_NAME','sae');
// 定义项目名称和路径
define ( 'APP_NAME', 'App' );
define ( 'APP_PATH', './App/' );

//定义ThinkPHP核心文件路径
define('THINK_PATH', './ThinkPHP/');

//定义网站根目录
define("WEB_ROOT", dirname(__FILE__) . "/");

//强制修正URL路径(一般不需要开启)
//define('__APP__', '');


// 定义项目RUNTIME路径
define('RUNTIME_PATH', WEB_ROOT . "Data/Temp/");

// 定义项目日志文件路径
define('LOG_PATH', WEB_ROOT . "Data/Log/");

//定义Cache目录
define('WEB_CACHE_PATH', WEB_ROOT . "Data/Cache/");

//系统备份数据库文件存放目录
define("DatabaseBackDir", WEB_ROOT . "Data/DBbackup/");

//系统备份文件存放目录
define("SystemBackDir", WEB_ROOT . "Data/Backup/");

//系统升级文件存放目录
define("UpgradeDir", WEB_ROOT . "Data/Upgrade/");

//上传文件存放目录

define("UploadDir",  "Upload/");

//定义网站调试状态
define ( 'APP_DEBUG', false );

//定义数据库信息
require(WEB_ROOT . "db_config.php");

// 加载框架入口文件
require(THINK_PATH . "ThinkPHP.php");
