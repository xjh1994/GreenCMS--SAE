-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- 主机: 127.0.0.1
-- 生成日期: 2014 年 01 月 02 日 14:05
-- 服务器版本: 5.5.27
-- PHP 版本: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `greenstudio`
--

-- --------------------------------------------------------

--
-- 表的结构 `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `role_id` smallint(6) unsigned NOT NULL DEFAULT '1',
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限分配表';

--
-- 转存表中的数据 `access`
--

INSERT INTO `access` (`role_id`, `node_id`, `level`, `pid`, `module`) VALUES
(1, 1, 1, 0, NULL),
(1, 2, 2, 1, NULL),
(1, 5, 3, 2, NULL),
(1, 6, 3, 2, NULL),
(1, 3, 2, 1, NULL),
(1, 7, 3, 3, NULL),
(1, 4, 2, 1, NULL),
(1, 10, 3, 4, NULL),
(1, 11, 3, 4, NULL),
(1, 12, 3, 4, NULL),
(1, 13, 3, 4, NULL),
(1, 14, 2, 1, NULL),
(1, 8, 3, 14, NULL),
(1, 9, 3, 14, NULL),
(1, 15, 3, 14, NULL),
(1, 16, 3, 14, NULL),
(1, 17, 3, 14, NULL),
(1, 18, 3, 14, NULL),
(1, 19, 3, 14, NULL),
(1, 20, 3, 14, NULL),
(1, 21, 3, 14, NULL),
(1, 22, 3, 14, NULL),
(1, 23, 3, 14, NULL),
(1, 24, 3, 14, NULL),
(1, 25, 3, 14, NULL),
(1, 49, 3, 14, NULL),
(1, 50, 3, 14, NULL),
(1, 51, 3, 14, NULL),
(1, 26, 2, 1, NULL),
(1, 27, 3, 26, NULL),
(1, 28, 3, 26, NULL),
(1, 29, 3, 26, NULL),
(1, 30, 3, 26, NULL),
(1, 31, 3, 26, NULL),
(1, 45, 3, 26, NULL),
(1, 32, 2, 1, NULL),
(1, 33, 3, 32, NULL),
(1, 34, 3, 32, NULL),
(1, 35, 3, 32, NULL),
(1, 36, 3, 32, NULL),
(1, 37, 3, 32, NULL),
(1, 38, 3, 32, NULL),
(1, 39, 3, 32, NULL),
(1, 40, 3, 32, NULL),
(1, 41, 3, 32, NULL),
(1, 42, 3, 32, NULL),
(1, 43, 3, 32, NULL),
(1, 44, 3, 32, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `cats`
--

CREATE TABLE IF NOT EXISTS `cats` (
  `cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cat_father` bigint(10) NOT NULL DEFAULT '0',
  `cat_slug` varchar(200) NOT NULL DEFAULT '',
  `cat_name` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`cat_id`),
  UNIQUE KEY `slug` (`cat_slug`),
  KEY `name` (`cat_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='分类信息表' AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- 表的结构 `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_content` text NOT NULL,
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`),
  KEY `comment_parent` (`comment_parent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='评论信息表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `comments`
--

INSERT INTO `comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_content`, `comment_approved`, `comment_parent`, `user_id`) VALUES
(1, 1, 'WordPress 先生', '', 'http://wordpress.org/', '', '2013-01-24 00:23:55', '您好，这是一条评论。要删除评论，请先登录，然后再查看这篇文章的评论。登录后您可以看到编辑或者删除评论的选项。', '1', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `fid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'anonymous',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Waiting',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='反馈信息' AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `feedback`
--

INSERT INTO `feedback` (`fid`, `name`, `email`, `comment`) VALUES
(1, 'anonymous', 'admin@zts1993.com', 'Waiting'),
(2, '在', 'admin@nutlab.asia', '当时Vad'),
(3, '在', 'admin@nutlab.asia', '当时Vad'),
(4, '阿三地方', 'a8580160@163.com', 'asd'),
(5, 'Facebook效应', 'admin0@admin0.com', 'sdf'),
(6, 'anonymous', 'admin@zts1993.com', 'Waiting'),
(7, 'anonymous', 'zts1993@126.com', 'Waiting'),
(8, 'anonymous', 'zts1993@126.com', 'Waiting'),
(9, 'anonymous', 'admin@zts1993.com', 'Waiting');

-- --------------------------------------------------------

--
-- 表的结构 `form_apply`
--

CREATE TABLE IF NOT EXISTS `form_apply` (
  `fa_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `direction` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`fa_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='申请表' AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `form_apply`
--

INSERT INTO `form_apply` (`fa_id`, `name`, `class`, `tel`, `email`, `direction`, `message`) VALUES
(1, 'zhang tianshuo', '1234567891', '8618251898768', 'admin@zts1993.com', 'web', '                sdfasdaf'),
(2, 'zhang tianshuo', '1234567891', '8618251898768', 'admin@zts1993.com', 'web', '                sdfasdaf'),
(3, 'zhang tianshuo', '1234567891', '8618251898768', 'admin@zts1993.com', 'web', ''),
(4, '', '1234567891', '8618251898768', 'admin@zts1993.com', 'web', ''),
(5, 'zhang tianshuo', '1234567891', '8618251898768', 'admin@zts1993.com', 'bigdata', '                sdf '),
(6, 'fdf', 'dsf', 'sdf', 'sdf', '', '                asdfsadf');

-- --------------------------------------------------------

--
-- 表的结构 `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_sort` smallint(25) DEFAULT '0',
  `link_url` varchar(255) DEFAULT '',
  `link_name` varchar(255) DEFAULT '',
  `link_tag` varchar(255) DEFAULT '',
  `link_description` varchar(255) DEFAULT '',
  `link_visible` tinyint(20) DEFAULT '1',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='友情链接' AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `links`
--

INSERT INTO `links` (`link_id`, `link_sort`, `link_url`, `link_name`, `link_tag`, `link_description`, `link_visible`) VALUES
(1, 1, 'http://www.zts1993.com', 'Z的博客', '', 'Z的博客', 1);

-- --------------------------------------------------------

--
-- 表的结构 `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_pid` bigint(20) NOT NULL DEFAULT '0',
  `menu_sort` bigint(20) NOT NULL DEFAULT '99',
  `menu_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `menu_module` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `menu_fuction` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `menu_position` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `menu_url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='菜单栏' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `node`
--

CREATE TABLE IF NOT EXISTS `node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='权限节点表' AUTO_INCREMENT=54 ;

--
-- 转存表中的数据 `node`
--

INSERT INTO `node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES
(1, 'Admin', '后台管理', 1, '网站后台管理项目', 0, 0, 1),
(2, 'Index', '管理首页', 1, '', 0, 1, 2),
(53, 'addlink', '添加链接', 1, '添加链接', 0, 4, 3),
(4, 'Webinfo', '系统设置', 1, '系统设置', 0, 1, 2),
(5, 'index', '默认页', 1, '', 0, 2, 3),
(6, 'myInfo', '我的个人信息', 1, '', 0, 2, 3),
(8, 'index', '管理员列表', 1, '', 0, 14, 3),
(9, 'addUserHandle', '添加用户处理', 1, '', 0, 14, 3),
(10, 'index', '系统设置首页', 1, '', 0, 4, 3),
(11, 'setEmailConfig', '设置系统邮件', 1, '', 0, 4, 3),
(12, 'testEmailConfig', '发送测试邮件', 1, '', 0, 4, 3),
(13, 'setSafeConfig', '系统安全设置', 1, '', 0, 4, 3),
(14, 'Access', '权限管理', 1, '权限管理，为系统后台管理员设置不同的权限', 0, 1, 2),
(15, 'nodelist', '查看节点', 1, '节点列表信息', 0, 14, 3),
(16, 'rolelist', '角色列表查看', 1, '角色列表查看', 0, 14, 3),
(17, 'addRole', '添加角色', 1, '', 0, 14, 3),
(18, 'editRole', '编辑角色', 1, '', 0, 14, 3),
(19, 'opNodeStatus', '便捷开启禁用节点', 1, '', 0, 14, 3),
(20, 'opRoleStatus', '便捷开启禁用角色', 1, '', 0, 14, 3),
(21, 'editNode', '编辑节点', 1, '', 0, 14, 3),
(22, 'addNode', '添加节点', 1, '', 0, 14, 3),
(23, 'addUser', '添加用户', 1, '', 0, 14, 3),
(24, 'editUser', '编辑用户', 1, '', 0, 14, 3),
(25, 'changeRole', '权限分配', 1, '', 0, 14, 3),
(26, 'Posts', '资讯管理', 1, '', 0, 1, 2),
(27, 'index', '新闻列表', 1, '', 0, 26, 3),
(28, 'category', '文章分类管理', 1, '', 0, 26, 3),
(29, 'add', '发布文章', 1, '', 0, 26, 3),
(30, 'edit', '编辑文章', 1, '', 0, 26, 3),
(31, 'del', '删除信息', 1, '', 0, 26, 3),
(32, 'SysData', '数据库管理', 1, '包含数据库备份、还原、打包等', 0, 1, 2),
(33, 'index', '查看数据库表结构信息', 1, '', 0, 32, 3),
(34, 'backup', '备份数据库', 1, '', 0, 32, 3),
(35, 'restore', '查看已备份SQL文件', 1, '', 0, 32, 3),
(36, 'restoreData', '执行数据库还原操作', 1, '', 0, 32, 3),
(37, 'delSqlFiles', '删除SQL文件', 1, '', 0, 32, 3),
(38, 'sendSql', '邮件发送SQL文件', 1, '', 0, 32, 3),
(39, 'zipSql', '打包SQL文件', 1, '', 0, 32, 3),
(40, 'zipList', '查看已打包SQL文件', 1, '', 0, 32, 3),
(41, 'unzipSqlfile', '解压缩ZIP文件', 1, '', 0, 32, 3),
(42, 'delZipFiles', '删除zip压缩文件', 1, '', 0, 32, 3),
(43, 'downFile', '下载备份的SQL,ZIP文件', 1, '', 0, 32, 3),
(44, 'repair', '数据库优化修复', 1, '', 0, 32, 3),
(45, 'tag', '文章标签管理', 1, '', 0, 26, 3),
(49, 'addRoleHandle', '添加角色处理', 1, '添加角色处理', 0, 14, 3),
(50, 'addNodeHandle', '添加节点处理', 1, '', 0, 14, 3),
(51, 'delUser', '删除用户', 1, '', 0, 14, 3),
(52, 'cache', '缓存清理 ', 1, '', 0, 2, 3);

-- --------------------------------------------------------

--
-- 表的结构 `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='选项表' AUTO_INCREMENT=439 ;

--
-- 转存表中的数据 `options`
--

INSERT INTO `options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(418, 'site_url', 'http://green.njut.asia', 'yes'),
(419, 'title', '机智的院士', 'yes'),
(420, 'keywords', '院士', 'yes'),
(421, 'description', '院士', 'yes'),
(422, 'foot', 'By GreenStudio', 'yes'),
(423, 'users_can_register', '0', 'yes'),
(424, 'send_mail', '1', 'yes'),
(425, 'mail_server', '', 'yes'),
(426, 'mail_port', '', 'yes'),
(427, 'smtp_addr', '', 'yes'),
(428, 'smtp_name', 'admin', 'yes'),
(429, 'smtp_pass', 'www.xjh1994.com', 'yes'),
(430, 'smtp_test', '', 'yes'),
(431, 'php_safe_mode', '1', 'yes'),
(432, 'allow_url_fopen', '1', 'yes'),
(433, 'think_token', '1', 'yes'),
(434, 'db_fieldtype_check', '1', 'yes'),
(435, 'smtp_host', 'smtp.qq.com', 'yes'),
(436, 'smtp_port', '25', 'yes'),
(437, 'smtp_user', '1412128697@qq.com', 'yes'),
(438, 'from_email', '1412128697@qq.com', 'yes');

-- --------------------------------------------------------

--
-- 表的结构 `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL,
  `post_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  `view_count` bigint(20) NOT NULL DEFAULT '0',
  `post_type` varchar(255) NOT NULL DEFAULT 'single',
  `post_img` varchar(255) DEFAULT NULL,
  `post_top` smallint(6) DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_status`,`post_date`,`post_id`),
  KEY `post_author` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文章列表' AUTO_INCREMENT=97 ;

--
-- 转存表中的数据 `posts`
--

INSERT INTO `posts` (`post_id`, `user_id`, `post_date`, `post_content`, `post_title`, `post_status`, `comment_status`, `post_password`, `post_name`, `post_modified`, `comment_count`, `view_count`, `post_type`, `post_img`, `post_top`) VALUES
(96, 1, '2013-10-14 01:10:46', '<p><iframe class="edui-faked-webapp" title="快递查询" width="560" height="478" scrolling="no" frameborder="0" src="http://app.baidu.com/app/enter?appid=101074&amp;tn=app_canvas&amp;app_spce_id=1&amp;apikey=nF7CUXca5xztsG1vnicM0efe&amp;api_key=nF7CUXca5xztsG1vnicM0efe" logo_url="http://apps3.bdimg.com/store/static/kvt/6fa4c5027ce68db227e960a2ea73d427.gif"></iframe></p><p>附件测试<br/></p><p style="line-height: 16px;"><img src="/greencms/Public/Ueditor/dialogs/attachment/fileTypeImages/icon_rar.gif"/><a href="/greencms/Public/Ueditor/php/upload/20131014/1381713328584.zip">4.1.2nfc.zip</a></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133553151.png" style="float:none;" title="6101.png"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133555189.png" style="float:none;" title="6560.png"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133555263.jpg" style="float:none;" title="0752270391241982.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133551873.jpg" style="float:none;" title="1151500140958263.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133579127.jpg" style="float:none;" title="IMG_20110930_151508.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133582126.jpg" style="float:none;" title="IMG_20110930_151637.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133594549.jpg" style="float:none;" title="IMG_20110930_151737.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133614660.jpg" style="float:none;" title="IMG_20110930_151956.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133621020.jpg" style="float:none;" title="IMG_20110930_152131.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133634122.jpg" style="float:none;" title="IMG_20110930_152154.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133655221.jpg" style="float:none;" title="IMG_20110930_152402.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133668599.jpg" style="float:none;" title="IMG_20110930_152408.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133688519.jpg" style="float:none;" title="IMG_20110930_152419.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133697114.jpg" style="float:none;" title="IMG_20110930_152431.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133714468.jpg" style="float:none;" title="IMG_20110930_152457.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133727128.jpg" style="float:none;" title="IMG_20110930_152509.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133736311.jpg" style="float:none;" title="IMG_20110930_152541.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133752040.jpg" style="float:none;" title="IMG_20110930_152846.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133764477.jpg" style="float:none;" title="IMG_20110930_152853.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133788685.jpg" style="float:none;" title="IMG_20110930_153235.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133794083.jpg" style="float:none;" title="IMG_20110930_154108.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133804836.jpg" style="float:none;" title="IMG_20110930_154942.jpg"/></p><p><img src="/greencms/Public/Ueditor/php/upload/20131014/13817133822434.jpg" style="float:none;" title="IMG_20110930_161839.jpg"/></p><p><br/></p>', '附件测试', 'publish', 'open', '', '附件测试', '2013-10-14 01:10:19', 0, 0, 'single', NULL, 0);

-- --------------------------------------------------------

--
-- 表的结构 `post_cat`
--

CREATE TABLE IF NOT EXISTS `post_cat` (
  `pc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cat_id` bigint(20) NOT NULL DEFAULT '0',
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章分类' AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- 表的结构 `post_meta`
--

CREATE TABLE IF NOT EXISTS `post_meta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章meta' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `pt_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) NOT NULL DEFAULT '0',
  `post_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='文章标签' AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- 表的结构 `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='角色' AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `role`
--

INSERT INTO `role` (`id`, `name`, `pid`, `status`, `remark`) VALUES
(1, '超级管理员', 1, 1, '系统内置超级管理员组'),
(2, '网站管理员', 2, 1, '拥有系统仅此于超级管理员的权限'),
(3, '内容管理员', 3, 1, '拥有发布文章权利'),
(5, '游客', 5, 1, '基本信息修改'),
(4, '投稿员', 4, 1, '只能投稿默认为未审核'),
(11, '测试员', -1, 1, '测试员');

-- --------------------------------------------------------

--
-- 表的结构 `role_users`
--

CREATE TABLE IF NOT EXISTS `role_users` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` bigint(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色表';

--
-- 转存表中的数据 `role_users`
--

INSERT INTO `role_users` (`role_id`, `user_id`) VALUES
(1, 1),
(11, 15);

-- --------------------------------------------------------

--
-- 表的结构 `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(200) NOT NULL DEFAULT '',
  `tag_slug` varchar(200) NOT NULL DEFAULT '',
  `tag_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `slug` (`tag_slug`),
  KEY `name` (`tag_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='标签页' AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_pass` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8 DEFAULT '',
  `user_registered` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_activation_key` varchar(60) CHARACTER SET utf8 DEFAULT '',
  `user_status` int(11) DEFAULT '1',
  `display_name` varchar(250) CHARACTER SET utf8 DEFAULT '',
  `user_intro` text CHARACTER SET utf8,
  `user_level` smallint(6) NOT NULL DEFAULT '10',
  PRIMARY KEY (`user_id`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户表' AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`user_id`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `user_intro`, `user_level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '管理员', 'admin@xjh1994.com', 'http://blog.xjh1994.com', '2013-01-24 00:23:55', '', 1, 'admin', '我是admin，欢迎使用', 2),
(15, 'test', 'ddf63c1a8f2492aa4e41b34e93a9d54e', '测试员', 'test@njut.asia', 'http://green.njut.asia', '2013-10-12 09:40:58', '', 1, '测试员', 'test', 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
