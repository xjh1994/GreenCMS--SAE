<?php

if (!defined('THINK_PATH'))
    exit();
// 系统别名定义文件
return array(
    'PHPMailer' => WEB_ROOT . APP_NAME . '/Common/Extend/PHPMailer/PHPMailer.class.php',
	'File' => WEB_ROOT . APP_NAME . '/Common/Extend/File.class.php',
    'CheckCode' => WEB_ROOT . APP_NAME . '/Common/Extend/CheckCode/Checkcode.class.php',
    'QRCode' => WEB_ROOT . APP_NAME . '/Common/Extend/QRCode.class.php',
    'Category' => WEB_ROOT . APP_NAME . '/Common/Extend/Category.class.php',
    'PHPZip' => WEB_ROOT . APP_NAME . '/Common/Extend/PHPZip.class.php',
);