<?php

class HomeWidget extends Action {
	
	public function home_footer($data = '') {

		
		$this->newPosts=D ( 'Posts' )->post_list ( 'post_date desc',5,'single',false ); 
		$this->friendurl= D ( 'Links' )->get_links_list ( '5' ); 
		
		$this->display ( 'Widget:home_footer' );
	
	}
	
	public function home_header($data = '') {
		
		$CatList = D ( 'Cats' );
		$cat_res =$CatList->limit(15)->select ();
		
		$PageList = D ( 'Posts' );
		$info['post_type']='page';
		$info['post_status']='publish';
		$page_res=$PageList->where ( $info )->relation(true)->select();
				
		$this->assign('cats',$cat_res);
		$this->assign('pages',$page_res);
			
		$this->display ( 'Widget:home_header' );
	
	}
	
	function categories($data = '')  {
		$CatList = D ( 'Cats' );

		$this->assign('list',D("Cats")->category());
		
		$this->display ( 'Widget:home_categories' );
	}
	
	
	function tag($data = '')  {
		$TagList = D ( 'Tags' );
		
		$tag_res =$TagList->limit(50)->select ();
		
		$this->assign('tagClouds',$tag_res);
	
		$this->display ( 'Widget:home_tag' );
	}
	
	function search($data = '')  {
	
		$this->display ( 'Widget:home_search' );
	}

	
	function aboutus($data = '')  {
	
		$this->display ( 'Widget:home_aboutus' );
	}
	
	function comment($data = '')  {
	
		$this->display ( 'Widget:duoshuo_comment' );
	}
	


}

?>
