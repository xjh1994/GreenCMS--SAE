<?php
//公共控制器
class BaseAction extends Action {
	function __construct() {
		parent::__construct();
		import ( '@.ORG.Plugin');
		$this->autload_config();
	}
	
	function get__config() {
		$options=D('Options')->where(array('autoload'=>'yes'))->select();
		return $options;
	}
	
	function autload_config() {
		
		$options=$this->get__config();
		foreach ($options as $config){
			C($config['option_name'],$config['option_value']);
		}
	
	}
	
}
