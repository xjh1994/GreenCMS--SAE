<?php
class TagAction extends CommonAction {
	
	function __construct() {
        parent::__construct();
		
	}
	
	
	public function index() {
		$TagList = D ( 'Tags' );
		$PosttagList = D ( 'Post_tag' );
		$tag_info ['tag_id|tag_slug'] = urlencode ( urldecode ( $_GET ['info'] ) );
		$tag_res =$TagList->field('tag_id,tag_name')->where ( $tag_info )->find ();
		if(!empty($tag_res)){
			//echo $tag_res['tag_id'];
			$post_res=$PosttagList->field('post_id')->where ( $tag_res )->select();
			$post_arrat=array();
			foreach($post_res AS $value){
				$id=$value['post_id'];array_push($post_arrat,$id);
			}
		}else{
			$this->error("没有这个标签");
		}
	
		$info ['post_id'] = array('in',$post_arrat);
		$info ['post_type'] = 'single';
		$info ['post_status'] = 'publish';
		
	import ( '@.ORG.Page' ); // 导入分页类
	$PostsList = D ( 'Posts' );
	$count = $PostsList-> where ( $info )->count(); // 查询满足要求的总记录数
	$Page = new Page ( $count, Pager ); // 实例化分页类 传入总记录数
	$show = $Page->show ();
	$res = $PostsList->where ( $info )->relation(true)->order ( 'post_id desc' )->limit ( $Page->firstRow . ',' . $Page->listRows )->select ();

	
		
	if(empty($res)){
		$res404=0;
	}else{
		$res404=1;
	}
	
	$this->assign ( 'title', $tag_res['tag_name'] ); // 赋值数据集
	$this->assign ( 'res404', $res404 ); // 赋值数据集
	
	
	$this->assign ( 'postslist', $res ); // 赋值数据集
	$this->assign ( 'page', $show ); // 赋值分页输出
		
		$this->display ( 'Archive/single-list' );
	
	}

}