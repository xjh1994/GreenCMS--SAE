<?php

class ArchiveAction extends CommonAction {
	
	function __construct() {
		parent::__construct ();
		
		import ( "@.ORG.Page" );
	}
	
	
	public function feature($id) {
	
		$CatList = D ( 'Cats' );
		
		print_array($CatList->get_posts($id));
		
		//print_array($CatList->childen($id));
	
		//二级分类目录
	}
	
	
	public function single() {
		
		
		$PostsList = D ( 'Posts' );
		
		$count = $PostsList->count_all(); // 查询满足要求的总记录数
		($count==0)?$res404 = 0:$res404 = 1;
		
		$Page = new Page ( $count, Pager ); // 实例化分页类 传入总记录数
		$navi = $Page->show ();

		$res = $PostsList->post_list_page($Page);

		
		$this->assign ( 'title', '所有文章' );
		$this->assign ( 'res404', $res404 ); // 赋值数据集
		$this->assign ( 'postslist', $res ); // 赋值数据集
		$this->assign ( 'page', $navi ); // 赋值分页输出
		
		$this->display ( 'single-list' );
	
	}
	
	

	
	
	public function page() {
		
		$PostsList = D ( 'Posts' );
		
		$count = $PostsList->count_all('page'); // 查询满足要求的总记录数
		($count==0)?$res404 = 0:$res404 = 1;
		
		$Page = new Page ( $count, Pager ); // 实例化分页类 传入总记录数
		$navi = $Page->show ();

		$res = $PostsList->post_list_page($Page,'page');
		
		$this->assign ( 'title', '所有页面' );
		$this->assign ( 'res404', $res404 ); // 赋值数据集
		$this->assign ( 'postslist', $res ); // 赋值数据集
		$this->assign ( 'page', $navi ); // 赋值分页输出
		
		$this->display ( 'page-list' );
	
	}

}