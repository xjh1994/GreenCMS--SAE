<?php
//公共控制器
class CommonAction extends BaseAction {
	function __construct() {
		parent::__construct();
		if (C('Maintain')==true)
			redirect ( U ( 'Maintain/index' ) );
	}
	
	
	function _initialize() {
		header("Content-Type:text/html; charset=utf-8");
	}
	/*空操作*/
	function _empty() {
		$this->error("抱歉您请求的页面不存在",U('Public/error404'));
	}
	
	function error404() {
		$this->error("抱歉您请求的页面不存在或已经删除。",U('Public/error404'));
	}
	
	function is404($res){
		if(empty($res)){
			$this->error404();
		}
	}
	
	
}
