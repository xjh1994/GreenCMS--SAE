<?php

class SingleAction extends CommonAction {
	
	
	function __construct() {
		parent::__construct ();
	
	}
	
	function _initialize() {
		
		
	}
	
	public function index() {
	
	}
	
	public function detail($info=-1) {
		$Posts = D ( 'Posts' );
		//print_array($_GET ['info']);
		$post_res = $Posts->detail ($info);
		
		$this->is404 ( $post_res );
		
		$this->assign ( 'post', $post_res ); // 赋值数据集
		
		$this->display ( 'detail' );
	}

}