<?php
class CatAction extends CommonAction {
	
	function __construct() {
		parent::__construct ();
		import ( "@.ORG.Page" );
	
	}
	
	public function index() {
		$CatList = D ( 'Cats' );
		$PostcatList = D ( 'Post_cat' );
		$PostsList = D ( 'Posts' );
		
		$cat_info ['cat_id|cat_slug'] = urlencode ( urldecode ( $_GET ['info'] ) );
		$cat_res = $CatList->field ( 'cat_id,cat_name' )->where ( $cat_info )->find ();
		
		if (! empty ( $cat_res )) {
			// echo $cat_res['cat_id'];
			$post_res = $PostcatList->field ( 'post_id' )->where ( $cat_res )->select ();
			$post_arrat = array ();
			foreach ( $post_res as $value ) {
				$id = $value ['post_id'];
				array_push ( $post_arrat, $id );
			}

			
		} else {
			$this->error404 ( "没有这个分类" );
		}
	
		$info ['post_id'] = array (
				'in',
				$post_arrat 
		);
		$info ['post_type'] = 'single';
		$info ['post_status'] = 'publish';
		
		
		
		$count = $PostsList->where ( $info )->count (); // 查询满足要求的总记录数
		($count==0)?$res404 = 0:$res404 = 1;
		
		$Page = new Page ( $count, Pager ); // 实例化分页类 传入总记录数
		$show = $Page->show ();
	
		
		$res = $PostsList->post_list_page($Page,'single', 'post_id desc','publish',$info)  ;
		
		$this->assign ( 'title', $cat_res ['cat_name'] ); // 赋值数据集
		$this->assign ( 'res404', $res404 ); // 赋值数据集
		
		$this->assign ( 'postslist', $res ); // 赋值数据集
		$this->assign ( 'page', $show ); // 赋值分页输出
		
		$this->display ( 'Archive:single-list' );
	
	}
	
	

	
	

}