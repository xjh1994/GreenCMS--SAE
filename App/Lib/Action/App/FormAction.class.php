<?php

/**
 * @author TianShuo
 *
 */
class FormAction extends BaseAction {
    
  
	function __construct() {
        parent::__construct();
	
	}
	
	
    public function apply(){
    	if (IS_POST) {
    		
    		//print_array($_POST);
    		
    		$Form_apply = D('Form_apply');
    		$Form_apply ->create();
    		
    		if (!$Form_apply->create()){
    			// 如果创建失败 表示验证没有通过 输出错误提示信息
    			$this->error('提交信息出错:'.$Form_apply->getError());
    			;
    		}else{
    			// 验证通过 可以进行其他数据操作
    		}
    		
    		
    		if ($Form_apply->add()) {
    			$this->success("提交成功",U('/'));
    		}else {
    			$this->error('提交信息出错');
    		}
    		
    		
    	}else
		$this->display();
    
    }
}