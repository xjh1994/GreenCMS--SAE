<?php
class FeedAction extends CommonAction {
	// 首页
	function __construct() {
		parent::__construct ();
		if (C ( 'Maintain' ) == true)
			redirect ( U ( 'Maintain/index' ) );
	}
	
	public function index() {

		$post_list =D( 'Posts' )-> post_list();
		import ( "@.ORG.Rss" );
		$RSS = new RSS ( C ( 'ALL_TITLE' ), '', C ( 'ALL_TITLE' ), '' ); // 站点标题的链接
		foreach ( $post_list as $list ) {
			$RSS->AddItem ( 
					$list ['post_title'],
					 
					$list ['post_id'] , 
					
					$list ['post_content'], 
					
					$list ['post_date'] );
		}
		
		$RSS->Display ();
	}
}
?>