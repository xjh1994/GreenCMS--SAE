﻿<?php
class CommonAction extends BaseAction {
	public $loginMarked;
	
	/**
	 * +----------------------------------------------------------
	 * 初始化
	 * 如果 继承本类的类自身也需要初始化那么需要在使用本继承类的类里使用parent::_initialize();
	 * +----------------------------------------------------------
	 */
	
	// public function __construct() {
	// parent::__construct();
	public function __construct() {
		parent::__construct();
		$this->_initialize();
	}
	public function _initialize() {
		Import ( '@.ORG.RBAC' );
		Import ( '@.ORG.File' );
		import ( '@.ORG.Plugin');
		if (! RBAC::AccessDecision ( 'Admin' )) 		// AccessDecision中间使用分组名
		{
			// 登录检查
			RBAC::checkLogin ();
			// 提示错误信息 无权限
			$this->error ( L ( '_VALID_ACCESS_' ) );
			// TODO 防止循环无权限
		}
		
		$this->assign ( 'head_meta', $this->head_meta () );
		$this->assign ( 'foot_js', $this->foot_js () );
		$this->assign ( 'footer', $this->footer () );
		
		$this->menu = $this->show_all_menu ();
		// $this->assign("sub_menu", $this->show_sub_menu());
		// $this->assign("submenu", $this->sub_menu());
		$this->current_postion ();
	//	echo C ( 'ADMIN_AUTH_KEY' );
		
		$user_id = ( int ) $_SESSION [C ( 'USER_AUTH_KEY' )];
		$user = D ( 'User' )->find ( $user_id );
		$this->username = $user ['user_nicename'];
	}
	
	/**
	 * +----------------------------------------------------------
	 * 验证token信息
	 * +----------------------------------------------------------
	 */
	protected function checkToken() {
		if (IS_POST) {
			if (! M ( "Admin" )->autoCheckToken ( $_POST )) {
				die ( json_encode ( array (
						'status' => 0,
						'info' => '令牌验证失败' 
				) ) );
			}
			unset ( $_POST [C ( "TOKEN_NAME" )] );
		}
	}
	
	/**
	 * +----------------------------------------------------------
	 * 显示菜单
	 * +----------------------------------------------------------
	 */
	private function show_all_menu() {
		C ( 'admin_sub_menu', array_change_key_case ( C ( 'admin_sub_menu' ) ) );
		C ( 'admin_big_menu', array_change_key_case ( C ( 'admin_big_menu' ) ) );
		
		$accessList = RBAC::getAccessList ( $_SESSION [C ( 'USER_AUTH_KEY' )] );
		$cache_access = array_change_key_case ( $accessList [strtoupper ( GROUP_NAME )] );
		
		$cache = C ( 'admin_big_menu' );
		if ($_SESSION [C ( 'ADMIN_AUTH_KEY' )] != true) {
			foreach ( $cache as $cache_key => $cache_each ) {
				if (! array_key_exists ( $cache_key, $cache_access )) {
					unset ( $cache [$cache_key] );
				} else {
				}
			}
		}
		
		if ($_SESSION [C ( 'ADMIN_AUTH_KEY' )] != true) {
			foreach ( $cache_access as $cache2_key => $cache2_each ) {
				foreach ( $cache2_each as $key => $value ) {
					$cache2_each [$key] = strtolower ( $cache2_key ) . '/' . strtolower ( $key );
				}
				$cache_access [$cache2_key] = array_flip ( array_change_key_case ( $cache2_each ) );
			}
			
			$cache2 = array_change_key_case ( C ( 'admin_sub_menu' ) );
			foreach ( $cache2 as $cache2_key => $cache2_each ) {
				$cache2 [$cache2_key] = array_change_key_case ( $cache2_each );
			}
			
			foreach ( $cache_access as $cache_access_key => $cache_access_each ) {
				foreach ( $cache_access_each as $cache_access_each_key => $cache_access_each_each ) {
					if (! empty ( $cache2 [$cache_access_key] [$cache_access_each_key] )) {
						$cache_access [$cache_access_key] [$cache_access_each_key] = $cache2 [$cache_access_key] [$cache_access_each_key];
					} else {
						unset ( $cache_access [$cache_access_key] [$cache_access_each_key] );
					}
				}
			}
			C ( 'admin_sub_menu', $cache_access );
		}
		
		$count = count ( $cache );
		$i = 1;
		$menu = "";
		foreach ( $cache as $url => $name ) {
			if ($i == 1) {

				$css = $url == strtolower ( MODULE_NAME ) || ! $cache [strtolower ( MODULE_NAME )] ? "start active" : "start";
				$menu .= '<li class="' . $css . '"><a href="javascript:;">
                <i class="icon-home"></i> 
                <span class="title">' . $name . '</span>
                <span class="arrow "></span>

                </a><ul class="sub-menu">';
				
				$cache = C ( 'admin_sub_menu' );
				foreach ( $cache as $big_url => $big_name ) {
					if ($big_url == $url)
						foreach ( $big_name as $sub_url => $sub_name ) {
							$sub_true_url = explode ( '/', $sub_url );
							$css = ! strcasecmp ( $sub_true_url [1], strtolower ( ACTION_NAME ) ) ? "active" : "";
							$menu .= '<li class="' . $css . '"><a href="' . U ( "$sub_url" ) . '">' . $sub_name . '</a></li>';
						}
				}
				$menu .= '</ul></li>';
			} else if ($i == $count) {

				$css = $url == strtolower ( MODULE_NAME ) ? "last active" : "last";
				$menu .= '<li class="' . $css . '"><a href="javascript:;">
                    <i class="icon-cogs"></i> 
                    <span class="title">' . $name . '</span>
                    <span class="arrow "></span>

                    </a><ul class="sub-menu">';
				
				$cache = C ( 'admin_sub_menu' );
				foreach ( $cache as $big_url => $big_name ) {
					if ($big_url == $url)
						foreach ( $big_name as $sub_url => $sub_name ) {
							$sub_true_url = explode ( '/', $sub_url );
							$css = ! strcasecmp ( $sub_true_url [1], strtolower ( ACTION_NAME ) ) ? "active" : "";
							$menu .= '<li class="' . $css . '"><a href="' . U ( "$sub_url" ) . '">' . $sub_name . '</a></li>';
						}
				}
				$menu .= '</ul></li>';
			} else {
				$css = $url == strtolower ( MODULE_NAME ) ? "start active" : "";
				$menu .= '<li class="' . $css . '"><a href="javascript:;">
                    <i class="icon-cogs"></i> 
                    <span class="title">' . $name . '</span>
                    <span class="arrow "></span>

                    </a><ul class="sub-menu">';
				$cache = C ( 'admin_sub_menu' );
				foreach ( $cache as $big_url => $big_name ) {
					if ($big_url == $url)
						foreach ( $big_name as $sub_url => $sub_name ) {
							$sub_true_url = explode ( '/', $sub_url );
							$css = ! strcasecmp ( $sub_true_url [1], strtolower ( ACTION_NAME ) ) ? "active" : "";
							$menu .= '<li class="' . $css . '"><a href="' . U ( "$sub_url" ) . '">' . $sub_name . '</a></li>';
						}
				}
				$menu .= '</ul></li>';
			}
			$i ++;
		}
		return $menu;
	}
	
	/**
	 * +----------------------------------------------------------
	 * 获取当前所在位置
	 * +----------------------------------------------------------
	 */
	private function current_postion() {
		$cache = C ( 'admin_big_menu' );
		foreach ( $cache as $big_url => $big_name ) {
			if ($big_url == strtolower ( MODULE_NAME )) {
				$module = $big_name;
				$module_url = U ( "$big_url" . '/index' );
			}
		}
		
		$cache = C ( 'admin_sub_menu' );
		foreach ( $cache as $big_url => $big_name ) {
			if ($big_url == strtolower ( MODULE_NAME )) {
				foreach ( $big_name as $sub_url => $sub_name ) {
					$sub_true_url = explode ( '/', $sub_url );
					if (! strcasecmp ( $sub_true_url [1], strtolower ( ACTION_NAME ) )) {
						$action = $sub_name;
						$action_url = U ( "$sub_url" );
					}
				}
			}
		}
		
		$this->module = $module;
		$this->action = $action;
		$this->module_url = $module_url;
		$this->action_url = $action_url;
	}
	public function head_meta() {
		return '
    <!DOCTYPE html>
    <!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
    <!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
    <!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="' . C ( 'description' ) . '" name="description" />
    <meta content="' . C ( 'keywords' ) . '" name="keywords" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="__PUBLIC__/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="__PUBLIC__/admin/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="__PUBLIC__/admin/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="__PUBLIC__/admin/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="__PUBLIC__/admin/assets/plugins/data-tables/DT_bootstrap.css" />

    <link href="__PUBLIC__/admin/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />
    <link href="__PUBLIC__/admin/assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <link href="__PUBLIC__/admin/assets/css/pages/profile.css" rel="stylesheet" type="text/css" />
    <link href="__PUBLIC__/css/tab.css" rel="stylesheet" type="text/css" />

    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="__PUBLIC__/images/logo.png" />';
	}
	public function footer() {
		return '
        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="footer-inner">
                ' . C ( 'foot' ) . '
            </div>
            <div class="footer-tools">
                <span class="go-top">
                <i class="icon-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        ';
	}
	public function foot_js() {
		return '
    <!-- BEGIN CORE PLUGINS -->
    <script src="__PUBLIC__/admin/assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="__PUBLIC__/admin/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="__PUBLIC__/admin/assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
    <script src="__PUBLIC__/admin/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <script src="__PUBLIC__/admin/assets/plugins/excanvas.min.js"></script>
    <script src="__PUBLIC__/admin/assets/plugins/respond.min.js"></script>  
    <![endif]--> 
    <script src="__PUBLIC__/admin/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="__PUBLIC__/admin/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
    <script src="__PUBLIC__/admin/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="__PUBLIC__/admin/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="__PUBLIC__/admin/assets/scripts/app.js"></script>
    <script src="__PUBLIC__/admin/assets/scripts/table-managed.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <script src="__PUBLIC__/admin/assets/scripts/form-wizard.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script type="text/javascript" src="__PUBLIC__/admin/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="__PUBLIC__/js/functions.js"></script>
    <script type="text/javascript" src="__PUBLIC__/js/jquery.form.js"></script>
	<script charset="utf-8" src="__PUBLIC__/artDialog/jquery.artDialog.js?skin=green"></script>
	<script charset="utf-8" src="__PUBLIC__/artDialog/extend.js"></script>
';
	}
}
