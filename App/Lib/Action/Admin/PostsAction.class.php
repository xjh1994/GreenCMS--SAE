<?php

class PostsAction extends CommonAction {

	public function index($post_type='single', $post_status = 'publish') {

		$where = array('post_type' =>$post_type, 'post_status' => $post_status);
		$posts = D('Posts')->where($where)->relation(true)->order('post_date desc')->select();
		$this->posts = $posts;

		$this->display('index');
	}
	
	public function page() {
		
		$this->index(page);
	}

	
	public function add() {

		$this->cats = D('Cats')->select();
		$this->tags = D('Tags')->select();
		
		$this->assign("handle", "addHandle");
		$this->assign("publish", "发布");

		$this->display();
	}

	public function addHandle() {

		$post_id = $_POST['post_id'] ? (int) $_POST['post_id'] : false;
		$data['post_type'] = $_POST['post_type']?$_POST['post_type'] : 'single';
		$data['post_title'] = $_POST['post_title'];
		$data['post_content'] = $_POST['post_content'];
		$data['post_name'] = $_POST['post_title'];
		$data['post_modified'] = $data['post_date'] = date("Y-m-d H:m:s", time());
		$data['user_id'] = $_SESSION [C ( 'USER_AUTH_KEY' )];
		
		if($post_id) {
			if (D('Posts')->where(array('id' => $post_id))->save($data)) {
				if ($data['post_type']=='single') {
					$this->success('修改成功', U('Admin/Posts/index'));
				}else {
					$this->success('修改成功', U('Admin/Posts/page'));
				}
                
            } else {
                $this->error('修改失败'); 
            }
		}
		else {
			if($post_id = D('Posts')->relation(true)->add($data)) {
				foreach ($_POST['cats'] as $cat_id) {
					M("Post_cat")->add(array("cat_id" => $cat_id, "post_id" => $post_id));
				}
				foreach ($_POST['tags'] as $tag_id) {
					M("Post_tag")->add(array("tag_id" => $tag_id, "post_id" => $post_id));
				}
				// $this->success('发布成功', U('Admin/Posts/index'));
				
				if ($data['post_type']=='single') {
					die(json_encode(array("status" => 1, "info" => "发布成功", "url" => U('Admin/Posts/index'))));
				}else {
					die(json_encode(array("status" => 1, "info" => "发布成功", "url" => U('Admin/Posts/page'))));
				}
				//die(json_encode(array("status" => 1, "info" => "发布成功", "url" => U('Admin/Posts/index'))));
			}
		}
	}

	//删除到回收站
	public function del($id=0) {
	
		$data['post_status'] = 'unpublished';		
		if (M('Posts')->where(array('post_id' => $id))->setField($data)) {
			$this->success('删除到回收站成功');
		}else {
			$this->error('删除到回收站失败');
		}
				
	}
	
	//永久删除
	public function per_del($id = 0) {
		
		if (D("Posts")->del($id)) {
			$this->success('永久删除成功');
		}else {
			$this->error('永久删除失败');
		}
	}

	//恢复
	public function recycle($post_type='single', $post_status = 'unpublished') {
	
		$where = array('post_type' =>$post_type, 'post_status' => $post_status);
		$posts = D('Posts')->where($where)->relation(true)->order('post_date desc')->select();
		$this->posts = $posts;
		
		$this->display();	
	}
	
	public function recycleHandle($id=0) {
		
		$data['post_status'] = 'publish';
		if (M('Posts')->where(array('post_id' => $id))->setField($data)) {
			$this->success('恢复成功');
		}else {
			$this->error('恢复失败');
		}
	}
	
	
	public function posts($id=-1) {

		$this->action = '编辑文章';
		$this->action_name = 'posts';
		$this->post_id = $post_id = $_GET['id'] ? (int)$_GET['id'] : false;
		$M = M("Posts");
		if(IS_POST) {
	        $data = $_POST;
	       //Log::write(array_to_str($data));
	        $data['post_modified'] = date("Y-m-d H:m:s", time());
	        $data['post_type'] = $_POST['post_type']?$_POST['post_type'] : 'single';
			M("post_cat")->where(array("post_id" => $data['post_id']))->delete();
			M("post_tag")->where(array("post_id" => $data['post_id']))->delete();
			
			if (!empty($_POST['cats'])) {
				 foreach ($_POST['cats'] as $cat_id) {
					M("post_cat")->add(array("cat_id" => $cat_id, "post_id" => $data['post_id']));
				};
			}
	       
			if (!empty($_POST['tags'])) {
				foreach ($_POST['tags'] as $tag_id) {
					M("post_tag")->add(array("tag_id" => $tag_id, "post_id" => $data['post_id']));
				}
			}
			
			if ($data['post_type']=='single') {
				$url= U('Posts/index');
			}else {
				$url= U('Posts/page');
			}
			
			
	        if ($M->save($data)) {
	            die(json_encode(array("status" => 1, "info" => "已经更新", "url" =>$url)));
	        } else {
	            die(json_encode(array("status" => 0, "info" => "更新失败", "url" => $url)));
	        }
		}
		else {
			
				$info = D('Posts')->relation(true)->where(array("post_id" => $post_id))->find();
				if (empty($info)) {
					$this->error("不存在该记录");
				}
				$this->cats = M('cats')->select();
				$this->tags = M('tags')->select();
	            $this->assign("info", $info);
	            $this->assign("handle", "posts");
	            $this->assign("publish", "更新");
	            $this->display('add');
		
		}

	}

	public function category() {

		$this->category = D("Cats")->relation(true)->category();
		
		$this->display();
	}

	public function addCategory() {
		$this->action = '添加';
		$this->cats = D('Cats')->category();

		$this->display();
	}
	
	public function addCategoryHandle() {

		$data['cat_name'] = htmlspecialchars(trim($_POST['cat_name']));
		$data['cat_slug'] = htmlspecialchars(trim($_POST['cat_slug']));
		$data['cat_father'] = htmlspecialchars(trim($_POST['cat_father']));

		if(D('Cats')->data($data)->add()) {
			$this->success('分类添加成功', U('Admin/Posts/category'));
		}else {
			$this->error('分类添加失败', U('Admin/Posts/category'));
		}
	}
	
	public function editCategory($id) {

		$this->action = '编辑';
		
		$this->cat = D('Cats')->find($id);
		
		$this->cats = D('Cats')->category();
	
		$this->display();
	}
	
	public function editCategoryHandle() {

		$id = $_GET['id'];

		$Cats=D('Cats');
		$data['cat_name'] = htmlspecialchars(trim($_POST['cat_name']));
		$data['cat_slug'] = htmlspecialchars(trim($_POST['cat_slug']));
		$data['cat_father'] = htmlspecialchars(trim($_POST['cat_father']));
		
		if($Cats->where(array('cat_id'=>$id))->save($data)) {
			
			$this->success('分类编辑成功', U('Admin/Posts/category'));
		}else {
			Log::write($Cats->getLastSql());
			$this->error('分类编辑失败', U('Admin/Posts/category'));
		}
	}

	
	public function delCategory($id=-1) {

		if($id == 1) {
			$this->error("默认分类不可删除");
		}
		else {
			if(D('Cats')->relation(true)->del($id)) {
				
				$data['cat_id'] = '1';
				if(M('Post_cat')->where(array("cat_id" => $id))->find()) {
					$post = M('Post_cat')->where(array("cat_id" => $id))->select();
					foreach($post as $v) {
						M('Post_cat')->where(array("pc_id" => $v['pc_id']))->data($data)->save();
					}
				}
					
				$this->success('分类删除成功', U('Admin/Posts/category'));
			}else {
				$this->success('分类删除失败:没有找到指定分类,可能它已经被删除',U('Admin/Posts/category'));
			}
		}
	}

	public function tag() {

		$this->tags = D('Tags')->select();
		
		$this->display();
	}

	public function addTag() {

		$this->display();
	}

	public function addTagHandle() {

		$data['tag_name'] = htmlspecialchars(trim($_POST['tag_name']));
		$data['tag_slug'] = htmlspecialchars(trim($_POST['tag_slug']));

		if(M('tags')->data($data)->add()) {
			$this->success('标签添加成功', U('Admin/Posts/tag'));
		}
	}
	
	public function editTag($id) {
		$this->action = '编辑';
		$this->tag = D('Tags')->find($id);
		$this->display();
	}
	
	public function editTagHandle() {

		$id = $_GET['id'];

		$Tags=D('Tags');
		$data['tag_name'] = htmlspecialchars(trim($_POST['tag_name']));
		$data['tag_slug'] = htmlspecialchars(trim($_POST['tag_slug']));
	
		if($Tags->where(array('tag_id'=>$id))->save($data)) {
				
			$this->success('分类编辑成功', U('Admin/Posts/tag'));
		}else {
			Log::write($Tags->getLastSql());
			$this->error('分类编辑失败~~可能没有更新', U('Admin/Posts/tag'));
		}
	}
	
	
	
	public function delTag($id=-1) {
	
		if(D('Tags')->relation(true)->delete($id)) {
			$this->success('标签删除成功', U('Admin/Posts/tag'));
		}else {
			$this->success('标签删除失败:没有找到指定标签,可能它已经被删除',U('Admin/Posts/tag'));
		}
	}
	
	
	
}