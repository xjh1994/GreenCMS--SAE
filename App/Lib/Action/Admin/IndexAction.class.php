<?php
class IndexAction extends CommonAction {
	public function index() {
		

	
		
		// $this->menu();
		$user_id = ( int ) $_SESSION [C ( 'USER_AUTH_KEY' )];
		$user = M ( 'user' )->find ( $user_id );
		$this->username = $user ['display_name'];
		$this->user_nicename = $user ['user_nicename'];
		$this->user_url = $user ['user_url'];
		$this->user_email = $user ['user_email'];
		$this->user_registered = $user ['user_registered'];
		$this->user_intro = $user ['user_intro'];
		
		$this->display ();
	}
	
	
	public function cache() {
		import ( "@.ORG.Dir" );
		$Dir = new Dir(RUNTIME_PATH);
			
		
		$caches = array (
				"HomeCache" => array (
						"name" => "网站缓存文件",
						"path" => RUNTIME_PATH . "Cache",
						"size" => $Dir->size(RUNTIME_PATH. "Cache"),
						
				),
				"HomeData" => array (
						"name" => "网站数据库字段缓存文件",
						"path" => RUNTIME_PATH . "Data" ,
						"size" => $Dir->size(RUNTIME_PATH. "Data"),
				),
				"AdminLog" => array (
						"name" => "网站日志缓存文件",
						"path" => RUNTIME_PATH . "Logs" ,
						"size" => $Dir->size(RUNTIME_PATH. "Logs"),
				),
				"AdminTemp" => array (
						"name" => "网站临时缓存文件",
						"path" => RUNTIME_PATH . "Temp" ,
						"size" => $Dir->size(RUNTIME_PATH. "Temp"),
						
				),
				"Homeruntime" => array (
						"name" => "网站~runtime.php缓存文件",
						"path" => RUNTIME_PATH . "~runtime.php" ,
						"size" =>  $Dir->realsize(RUNTIME_PATH. "~runtime.php"),
				) 
		);
		
		// p($_POST['cache']);die;
		if (IS_POST) {
			foreach ( $_POST ['cache'] as $path ) {
				if (isset ( $caches [$path] ))
					$Dir->delDirAndFile ( $caches [$path] ['path'] );
			}
			
			/*echo json_encode ( array (
					"status" => 1,
					"info" => "缓存文件已清除" 
			) );*/
			$this->success("清除成功");
		} else {
			
			
			
			//print_array(	 Dir::dirSize(RUNTIME_PATH));
			
			$this->assign ( "caches", $caches );
			 $this->display();
		 }
	 }

    public function myInfo() {

    	
    	if (IS_POST) {
    	  $user = M('user');
	        $user->user_id = (int)$_SESSION [C ( 'USER_AUTH_KEY' )];
	        $user->user_pass = md5($_POST['password']);
	        
	        if($user->save()) {
	        	redirect(U ( 'Admin/Login/logout' ),3,'密码修改成功');
	        }else {
	        	$this->error('密码修改失败');

	        }
    	}
        $this->display();
    }


}
