<?php 
class OptionsModel extends Model {
	
	protected $fields = array(
		
			'option_id',
			
			'option_name',
			
			'option_value',
			
			'autoload',
			
			'_pk' => 'option_id',
			
			'_autoInc' => true
	);
}

?>