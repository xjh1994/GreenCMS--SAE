<?php
class UserModel extends RelationModel {
	public $_link = array (
			
			'Role' => array (
					
					'mapping_type' => MANY_TO_MANY,
					
					'class_name' => 'Role',
					
					'mapping_name' => 'role',
					
					'foreign_key' => 'user_id',
					
					'relation_foreign_key' => 'role_id',
					
					'relation_table' => 'role_users' 
			),
			
			'Name' => array (
					
					'mapping_type' => BELONGS_TO,
					
					'class_name' => 'Role_users',
					
					'mapping_name' => 'name',
					
					'mapping_key' => 'user_id',
						
					'foreign_key' => 'user_id',
					
					'parent_key' => 'user_id',
			) 
	)
	;
	
	
	public function detail($uid) {
		$User= D ( 'User' );
		$user=$User->where(array('user_id'=>$uid))->find();
		return $user;		
	}
}
