<?php
if (!defined('THINK_PATH'))
	exit();

//数据库类型
define("GreenCMS_DB_TYPE", 'mysqli');

//数据库地址
define("GreenCMS_DB_HOST", 'w.rdc.sae.sina.com.cn');	//对应本地的127.0.0.1，以官方文档为准

//数据库名称
define("GreenCMS_DB_NAME", 'greenstudio');	//你的SAE数据库，通常以APP开头

//用户名
define("GreenCMS_DB_USR", 'root');	//SAE应用的access key:

//密码
define("GreenCMS_DB_PWD", '');          //SAE应用的secret key

//端口
define("GreenCMS_DB_PORT", '3307');	//SAE上默认是3307

//前缀,生产环境需要自形添加 如 green_
define("GreenCMS_DB_PREFIX", '');